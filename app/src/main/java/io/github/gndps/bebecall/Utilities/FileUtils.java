package io.github.gndps.bebecall.Utilities;

import android.os.Environment;

import java.io.File;
/***
 * Handles all the file system related operations for the app
 */

public class FileUtils {

        public static final String BASE_DIRECTORY = Environment.getExternalStorageDirectory().getPath();
        public static final String APP_FOLDER_NAME = "EVVA";
        public static final String TEMP_FOLDER_NAME = "temp";
        public static final String AUDIO_RECORDER_FILE_EXT_WAV = ".wav";
        public static final String AUDIO_RECORDER_TEMP_FILE = "record_temp.raw";

    public static String getTempWavFilePath(){

        String tempDir = getTempAppDirectory();
        long timeStamp = getTimeStamp();
        return (tempDir+ "/" + timeStamp + AUDIO_RECORDER_FILE_EXT_WAV);

    }

    public static String getTempRawFilePath(){
        String tempDir = getTempAppDirectory();
        return (tempDir + "/" + AUDIO_RECORDER_TEMP_FILE);
    }

    public static File getAppDirectory(){
        return  new File(BASE_DIRECTORY,APP_FOLDER_NAME);
    }

    public static String getAbsoluteAppDirectoryPath(){
        File file = getAppDirectory();
        getOrMkdir(file);
        return file.getAbsolutePath();
    }

    public static long getTimeStamp(){
        return  System.currentTimeMillis();
    }

    public static void getOrMkdir(File file){
        if(!file.exists()){
            file.mkdirs();
        }
    }

    public static void deleteTempRawFile(){
        String tempFilePath=getTempRawFilePath();
        File file = new File(tempFilePath);
        deleteFile(file);
    }

    public static void deleteFile(File file){
        if(file.exists()){
            file.delete();
        }
    }

    public static String getTempAppDirectory(){
        String absoluteAppDir = getAbsoluteAppDirectoryPath();
        File file = new File(absoluteAppDir,TEMP_FOLDER_NAME);
        getOrMkdir(file);
        return file.getAbsolutePath();
    }

    public static File getTempWavFile(){
        return new File(getTempWavFilePath());
    }

}
