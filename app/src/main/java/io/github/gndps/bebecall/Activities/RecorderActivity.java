package io.github.gndps.bebecall.Activities;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.method.ScrollingMovementMethod;
import android.widget.ImageView;
import android.widget.TextView;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

import io.github.gndps.bebecall.Core.Recording.AudioRecorder;
import io.github.gndps.bebecall.Obsolute.ContinousAudioRecorder;
import io.github.gndps.bebecall.R;

public class RecorderActivity extends AppCompatActivity {

    private static final String TAG = RecorderActivity.class.getSimpleName();

    private TextView activityLoggerTextView;
    private ImageView recorderImageView;
    private AudioRecorder recorderWrapper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recorder);

        getSupportActionBar().hide();

        initViews();

        configRecorder();
        recorderWrapper.start();

    }

    private void configRecorder(){
        recorderWrapper = new AudioRecorder(this,true);
        recorderWrapper.setActivityListener(new AudioRecorder.ActivityListener() {
            @Override
            public void onRecording(final String logs) {

                String currentTime = getCurrentTime();
                activityLoggerTextView.append("\n~["+currentTime+"]:"+logs);

            }

            @Override
            public void onAudioChunkPulled(final float amplitude) {

                animateVoice(amplitude);
            }

        });
    }



    public String getCurrentTime(){
    Calendar calendar = Calendar.getInstance(TimeZone.getTimeZone("GMT+5:30"));
    Date date = calendar.getTime();
    DateFormat dateFormat = new SimpleDateFormat("HH:mm a");

    dateFormat.setTimeZone(TimeZone.getTimeZone("GMT+5:30"));
    return dateFormat.format(date);
    }
    private void  initViews(){
        activityLoggerTextView = findViewById(R.id.activity_logger_textview);
        activityLoggerTextView.setMovementMethod(new ScrollingMovementMethod());


        recorderImageView = findViewById(R.id.recording_image_view);
    }

    private void animateVoice(float maxPeak) {
        recorderImageView.animate().scaleX(1 + maxPeak).scaleY(1 + maxPeak).setDuration(10).start();
    }

    @Override
    protected void onStop() {
        super.onStop();
        if(recorderWrapper!=null) {
            recorderWrapper.stop();
        }
    }
}
