package io.github.gndps.bebecall.Activities;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;

import io.github.gndps.bebecall.Fragments.NewHotwordFragment;
import io.github.gndps.bebecall.R;

public class HotWordActivity extends AppCompatActivity implements NewHotwordFragment.OnFragmentInteractionListener {

    private NewHotwordFragment mRecordingFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_hot_word_creation);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = findViewById(R.id.new_hotword_floating_action_button);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//               displayHotwordRecorderFragment();
            }
        });

        mRecordingFragment = NewHotwordFragment.newInstance();
        displayHotwordRecorderFragment();

    }

    private void displayHotwordRecorderFragment() {
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.add(R.id.fragment_container_frame_layout,mRecordingFragment);
        fragmentTransaction.commit();
    }

    @Override
    public void onFragmentInteraction() {

    }
}
