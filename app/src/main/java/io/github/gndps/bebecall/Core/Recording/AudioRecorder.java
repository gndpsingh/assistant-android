package io.github.gndps.bebecall.Core.Recording;

import android.app.Activity;
import android.support.annotation.NonNull;
import android.util.Log;


import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;


import io.github.gndps.bebecall.Core.Detection.HotwordDetector;
import io.github.gndps.bebecall.Utilities.FileUtils;
import omrecorder.AudioChunk;
import omrecorder.AudioRecordConfig;
import omrecorder.OmRecorder;
import omrecorder.PullTransport;
import omrecorder.PullableSource;
import omrecorder.Recorder;
import omrecorder.WriteAction;

/***
 *  This class is a wrapper for an audio recorder.
 *  The recorder starts recording when 'start()' method is called
 *  CORE : While recording the audio the recorder continuously produce .wav audio files, whenever there is a silence for
 *  1 second. a joint audio file is also produced by the underlying library(will have to get rid of it shortly)
 */

public class AudioRecorder {

    public static String TAG = AudioRecorder.class.getSimpleName();
    private Recorder recorder;
    private Activity activity;
    private ActivityListener listener;
    private HotwordDetector hotwordDetector;
    private AudioFileObserver observer;
    private RecorderStateListener stateListener;
    private boolean isContinuousRecordingMode;

    // Contains the path of final recorded file when recorder is stopped
    private String recordedFile;

    // Variable tracks weather the allowed duration for silence is crossed while recording continuously
    private boolean silenceThresholdCrossed;


    public AudioRecorder(final Activity activity, boolean isContinousRecordingMode) {

        this.activity = activity;
        this.isContinuousRecordingMode = isContinousRecordingMode;
        recordedFile = FileUtils.getTempWavFilePath();

        if (isContinousRecordingMode) {
            try {
                recorder = OmRecorder.wav(
                        new PullTransport.Noise(
                                mic(),
                                getAudioChunkPulledListener(),
                                new WriteAction.Default(),
                                getSilenceListener(),
                                SilenceDetector.SILENCE_ALLOWED_FOR
                        ), new File(recordedFile)
                );
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
            hotwordDetector = new HotwordDetector(activity, this);

        } else {
            recorder = OmRecorder.wav(
                    new PullTransport.Default(mic(), new PullTransport.OnAudioChunkPulledListener() {
                        @Override
                        public void onAudioChunkPulled(AudioChunk audioChunk) {
                            if (listener != null) {
                                listener.onAudioChunkPulled((float) (audioChunk.maxAmplitude() / 200.0));
                            }
                        }
                    }), new File(recordedFile));
        }
    }

    /***
     * The method onAudioChunkPulled provides the callback for the loop in which the recording is done by the underlying recorder
     * library.
     * CORE LOGIC : Recording while loop provides an audio chunk on each iteration which could be a piece of silence( Based on
     * silence threshold) or a piece of audio.
     * --> All pieces of audio are written to a temp raw file.
     * --> Silence which does not cross the allowed silence time limit is also written to temp file.
     * --> Whenever any silence crossing allowed silence time limit occurs, temp file is written to a .wav audio file.
     * --> temp file is deleted
     * ** Above steps repeats while(RecordingIsOn)
     */
    private PullTransport.OnAudioChunkPulledListener getAudioChunkPulledListener() throws FileNotFoundException {
        return new PullTransport.OnAudioChunkPulledListener() {

            String tempFile = FileUtils.getTempRawFilePath();

            // Output stream used to write to temp file
            FileOutputStream os = new FileOutputStream(tempFile);


            // Variable tracks the starting of an audio file and helps to trim the initial silence
            boolean isStartingOfAudio = true;


            @Override
            public void onAudioChunkPulled(AudioChunk audioChunk) {

                // check if the read audio chunk is silence or audio

                // if audiochunk is audio
                if (SilenceDetector.searchThreshold(audioChunk.toShorts(), SilenceDetector.SILENCE_THRESHOLD) > -1) {
                    try {

                        // write to output stream
                        os.write(audioChunk.toBytes());
                        isStartingOfAudio = false;
                        listener.onRecording("There was a sound..");
                    } catch (IOException e) {
                        e.printStackTrace();
                    }


                }
                // Audio chunk was a piece of silence
                else {


                    // If allowed silence duration is not crossed yet and the silence is not at the starting of some audio
                    if (!silenceThresholdCrossed && !isStartingOfAudio) {
                        try {

                            listener.onRecording("Silence less than threshold..");
                            os.write(audioChunk.toBytes());
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    } else {
                        listener.onRecording("Silence..");
                    }


                }

                // when allowed silence limit is crossed
                // write previously recorded data from temp file to .wav audio file
                if (silenceThresholdCrossed) {

                    listener.onRecording("Silence for so long..");
                    listener.onRecording("Creating wav file..");
                    String wavFileName = FileUtils.getTempWavFilePath();
                    WavAudio.create(tempFile, wavFileName);
                    observer.onWavFileSaved(wavFileName);
                    FileUtils.deleteTempRawFile();
                    try {
                        os.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    tempFile = FileUtils.getTempRawFilePath();
                    try {
                        os = new FileOutputStream(tempFile);
                    } catch (FileNotFoundException e) {
                        e.printStackTrace();
                    }
                    isStartingOfAudio = true;
                    silenceThresholdCrossed = false;
                } else {
                    //wait until there is a silence
                }
                listener.onAudioChunkPulled((float) (audioChunk.maxAmplitude() / 200.0));

            }
        };
    }

    private Recorder.OnSilenceListener getSilenceListener() {
        return new Recorder.OnSilenceListener() {
            @Override
            public void onSilence(long silenceTime) {

                listener.onRecording("Silence for a long time..");
                silenceThresholdCrossed = true;
                Log.e("silenceTime", String.valueOf(silenceTime));


            }
        };
    }

    @NonNull
    private PullableSource mic() {
        return new PullableSource.Default(
                new AudioRecordConfig.Default(
                        RecorderConfig.AUDIO_SOURCE, RecorderConfig.ENCODING,
                        RecorderConfig.CHANNEL, RecorderConfig.FREQUENCEY
                )
        );
    }

    public void start() {
        recorder.startRecording();

        if (isContinuousRecordingMode) {
            hotwordDetector.startDetection();
        }
    }

    public void stop() {
        try {
            recorder.stopRecording();
            if (!isContinuousRecordingMode) {
                observer.onWavFileSaved(recordedFile);
            }
            if (stateListener != null) {
                stateListener.onRecordingStopped(recordedFile);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void setActivityListener(ActivityListener activityListener) {
        this.listener = activityListener;
    }

    public void setAudioFileObserver(AudioFileObserver observer) {
        this.observer = observer;
    }

    public void setRecorderStateListener(RecorderStateListener listener) {
        stateListener = listener;
    }

    public interface ActivityListener {
        void onRecording(String logs);

        void onAudioChunkPulled(float amplitude);
    }

    public interface AudioFileObserver {
        void onWavFileSaved(String filename);
    }

    public interface RecorderStateListener {
        void onRecordingStopped(String filePath);
    }

}


