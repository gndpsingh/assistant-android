package io.github.gndps.bebecall.Core.Detection;

import android.app.Activity;
import android.util.Log;
import android.widget.Toast;

import com.snatik.storage.Storage;

import io.github.gndps.bebecall.Core.Recording.AudioRecorder;
import io.github.gndps.bebecall.Networking.ApiCaller;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Response;

public class HotwordDetector implements AudioRecorder.AudioFileObserver,AudioRecorder.RecorderStateListener {

    private Activity activity;
    public static final String TAG = HotwordDetector.class.getSimpleName();
    private Storage storage;
    private String mFileInProcessing;
    private AudioRecorder mAudioRecorder;
    private ApiCaller apiCaller;

    public HotwordDetector(Activity activity,AudioRecorder recorderWrapper){
        this.activity = activity;
        storage = new Storage(activity);
        mAudioRecorder = recorderWrapper;
        apiCaller = new ApiCaller(activity);
        configApiCaller();
    }

    public void startDetection(){

        mAudioRecorder.setAudioFileObserver(this);
        mAudioRecorder.setRecorderStateListener(this);

    }

    private void configApiCaller(){
        apiCaller.setApiCallListener(new ApiCaller.ApiCallListener() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                try {
                    Log.i("Response from backend:", response.toString());
                    int responseCode = Integer.parseInt(response.body().string());
                    Log.i("response code:", String.valueOf(responseCode));
                    actCool(responseCode);
                    // delete the processed file here
                    storage.deleteFile(mFileInProcessing);
                } catch (Exception e) {
                    Log.d(TAG, "chakko response - error ");
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Log.e(TAG, "call failed");
                storage.deleteFile(mFileInProcessing);
                t.printStackTrace();
            }
        });
        apiCaller.setDetectionApiFileInProcessingListener(new ApiCaller.DetectionApiFileInProcessingListener() {
            @Override
            public void onFileProcessed(String filePath) {
                mFileInProcessing = filePath;
            }
        });
    }


//    private void uploadFile(String filename) {
//        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
//        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
//        OkHttpClient client = new OkHttpClient.Builder().addInterceptor(interceptor).build();
//        mFileInProcessing = filename;
//        // Change base URL to your upload server URL.
//        UploadService service = RetrofitClientInstance.getRetrofitInstance().create(UploadService.class);
//
//        File file = new File(filename);
//        if(file.exists()){
//            Log.i(TAG, "uploadFile: file "+filename+" exists and uploading..");
//        }else{
//            Log.i(TAG, "uploadFile: file "+filename+" does not exists..");
//        }
//
//        RequestBody reqFile = RequestBody.create(MediaType.parse("multipart/form-data"), file);
//        MultipartBody.Part body = MultipartBody.Part.createFormData("file", file.getName(), reqFile);
//        RequestBody name = RequestBody.create(MediaType.parse("text/plain"), file.getName());
//
//        retrofit2.Call<okhttp3.ResponseBody> req = service.detectAudio(name, body);
//        req.enqueue(new Callback<ResponseBody>() {
//
//            @Override
//            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
//                try {
//                    Log.i("Response from backend:",response.toString());
//                    int responseCode = Integer.parseInt(response.body().string());
//                    Log.i("response code:",String.valueOf(responseCode));
//                    actCool(responseCode);
//                    // delete the processed file here
//                    storage.deleteFile(mFileInProcessing);
//                } catch (Exception e) {
//                    Log.d(TAG, "chakko response - error ");
//                }
//            }
//
//            @Override
//            public void onFailure(Call<ResponseBody> call, Throwable t) {
//                Log.e(TAG, "call failed");
//                storage.deleteFile(mFileInProcessing);
//                t.printStackTrace();
//            }
//
//
//        });
//    }

    private void actCool(int responseCode) {
        String msg;
        switch (responseCode) {
            case 1:
                //time ki hoya
                msg = "Time ki hoya hai";
                break;
            case 2:
                //happy nu phone la
                msg = "Happy nu phone la";
                break;
            case 3:
                //dk nu phone la
                msg = "dk nu phone la";
                break;
            case 4:
                //gundeep
                msg = "gundeep nu phone la";
                break;
            case 5:
                msg = "Kare aan";
                break;
            case 6:
                msg = "Negi ko phone kar";
                break;
            case 7:
                msg = "Snowboy";
                break;
            case 8:
                msg="smart mirror";
                break;
            case 9:
                msg= "Alexa";
                break;
            // oye
            case 10:
                msg="Han be";
                break;
            // time bata
            case 11:
                msg="time bata";
                break;
            // or bhai
            case 12:
                msg= "or bhai";
                break;

            default:
                msg = "Can't recognize";
        }
        Toast.makeText(activity, msg, Toast.LENGTH_SHORT).show();
    }


    @Override
    public void onWavFileSaved(String filename) {
        apiCaller.callDetectionApi(filename);
    }

    @Override
    public void onRecordingStopped(String filePath) {
        storage.deleteFile(filePath);

    }
}
