package io.github.gndps.bebecall.Interfaces;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;

public interface NewPmdlAPI {
    @Multipart
    @POST("pmdl/new")
    Call<ResponseBody> newPmdl(
            @Part("hotWordName") RequestBody hotWordName,
            @Part MultipartBody.Part file1,
            @Part MultipartBody.Part file2,
            @Part MultipartBody.Part file3
    );

}
