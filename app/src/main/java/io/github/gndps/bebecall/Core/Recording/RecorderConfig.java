package io.github.gndps.bebecall.Core.Recording;

import android.media.AudioFormat;
import android.media.MediaRecorder;


public class RecorderConfig {

    public static final int AUDIO_SOURCE=MediaRecorder.AudioSource.MIC;
    public static final int ENCODING=AudioFormat.ENCODING_PCM_16BIT;
    public static final int CHANNEL=AudioFormat.CHANNEL_IN_MONO;
    public static final int FREQUENCEY=16000;
    public static final int RECORDER_BPP = 16;

}
