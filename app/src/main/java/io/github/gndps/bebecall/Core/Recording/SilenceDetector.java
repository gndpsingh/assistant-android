package io.github.gndps.bebecall.Core.Recording;

public class SilenceDetector {

    public static short SILENCE_THRESHOLD =2700;
    public static long SILENCE_ALLOWED_FOR=200;

    public static int  searchThreshold(short[] arr, short thr){
        int peakIndex;
        int arrLen=arr.length;
        for (peakIndex=0;peakIndex<arrLen;peakIndex++){
            if ((arr[peakIndex]>=thr) || (arr[peakIndex]<=-thr)){
                // some value greater than the silence thresshold exists in the signal
                return peakIndex;
            }
        }
        return -1; //not found( Means this is silence as per the passed threshold value)
    }

}
