package io.github.gndps.bebecall.Interfaces;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;

/**
 * Created by Gundeep on 15/09/18.
 */

public interface DetectionApi {

    @Multipart
    @POST("detect")
    Call<ResponseBody> detectAudio(
            @Part("filename") RequestBody filename,
            @Part MultipartBody.Part file
    );

}
