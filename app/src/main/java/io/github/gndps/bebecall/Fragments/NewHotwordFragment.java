package io.github.gndps.bebecall.Fragments;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.snatik.storage.Storage;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import io.github.gndps.bebecall.Core.Recording.AudioRecorder;
import io.github.gndps.bebecall.Interfaces.NewPmdlAPI;
import io.github.gndps.bebecall.Networking.ApiCaller;
import io.github.gndps.bebecall.R;
import io.github.gndps.bebecall.Networking.RetrofitClientInstance;
import io.github.gndps.bebecall.Utilities.FileUtils;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link NewHotwordFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link NewHotwordFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class NewHotwordFragment extends Fragment implements View.OnClickListener {

    private EditText hotwordNameEditText;
    private Button recordHotwordButton;
    private Button resetRecordingsButton;
    private ImageView recorderImageView;
    private AudioRecorder audioRecorder;
    private boolean isRecording;
    private int recordedFileCount=0;
    private ProgressDialog mProgressDialog;
    private ApiCaller apiCaller;

//    private int currentlyPlayingAudio;
//    private boolean [] audioExists;
//
//
//
//    private LinearLayout playRecordingLinearLayout1;
//    private LinearLayout playRecordingLinearLayout2;
//    private LinearLayout playRecordingLinearLayout3;
//
//    private ImageView playRecordingImageView1;
//    private SeekBar recordingSeekBar1;
//    private ImageView deleteRecordingImageView1;
//
//    private ImageView playRecordingImageView2;
//    private SeekBar recordingSeekBar2;
//    private ImageView deleteRecordingImageView2;
//
//    private ImageView playRecordingImageView3;
//    private SeekBar recordingSeekBar3;
//    private ImageView deleteRecordingImageView3;
//
//    private Button createPmdlButton;
//
    private List<String> recordedFiles;
//
    private Storage storage;
//    private MediaPlayer mMediaPlayer;


//    private SeekBar.OnSeekBarChangeListener seekBarChangeListener = new SeekBar.OnSeekBarChangeListener() {
//        @Override
//        public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
//            if(mMediaPlayer != null && fromUser){
//                mMediaPlayer.seekTo(progress * 1000);
//            }
//        }
//
//        @Override
//        public void onStartTrackingTouch(SeekBar seekBar) {
//
//        }
//
//        @Override
//        public void onStopTrackingTouch(SeekBar seekBar) {
//
//        }
//    };

    public NewHotwordFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     * @return A new instance of fragment NewHotwordRecordingFragment.
     */

    public static NewHotwordFragment newInstance() {
        NewHotwordFragment fragment = new NewHotwordFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        recordedFiles = new ArrayList<>();

//        audioExists = new boolean[3];

        if (getArguments() != null) {
        // get the arguments from the bundle here
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_new_hotword_recording, container, false);
        initViews(view);
        return view;
    }

    private void initViews(View root){

        hotwordNameEditText=root.findViewById(R.id.new_hotword_name_edit_text);

        recordHotwordButton=root.findViewById(R.id.record_new_hotword);

        resetRecordingsButton = root.findViewById(R.id.reset_recordings_button);

        resetRecordingsButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(recordedFileCount!=0){
                    resetRecordedFiles();
                    Toast.makeText(getActivity(), "done!", Toast.LENGTH_SHORT).show();
                    recordHotwordButton.setText("Record new hotword");
                }else{
                    Toast.makeText(getActivity(), "Nothing to reset", Toast.LENGTH_SHORT).show();
                }
            }
        });

//        createPmdlButton = root.findViewById(R.id.create_pmdl_model_button);
//        createPmdlButton.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                 if(recordedFileCount<3){
//                     Toast.makeText(getActivity(), "Please record at least 3 files..", Toast.LENGTH_SHORT).show();
//                 }else{
//                     // Proceed to create pmdl here
//                 }
//            }
//        });

        recordHotwordButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!hotwordNameEditText.getText().toString().trim().equals("")) {
                    if (isRecording) {
                        audioRecorder.stop();
                        resetRecordingsButton.setVisibility(View.VISIBLE);
                        isRecording = false;
                    } else if (recordedFileCount < 3) {
                        configRecorder();
                        audioRecorder.start();
                        hotwordNameEditText.setVisibility(View.INVISIBLE);
                        resetRecordingsButton.setVisibility(View.INVISIBLE);
                        recordHotwordButton.setText("STOP RECORDING");
                        isRecording = true;
                    } else {
                        // create pmdl here
                        //Toast.makeText(getActivity(), "Pmdl will be created", Toast.LENGTH_SHORT).show();
                        apiCaller.callNewPmdlApi(hotwordNameEditText.getText().toString(),recordedFiles);
                        mProgressDialog.setMessage("Please wait");
                        mProgressDialog.show();

                    }
                }else{
                    hotwordNameEditText.setError("Please enter a name for new hotword first");
                }
            }
        });

        recorderImageView=root.findViewById(R.id.recording_image_view);

//
//        playRecordingLinearLayout1 = root.findViewById(R.id.play_recording_linear_layout1);
//        playRecordingLinearLayout2 = root.findViewById(R.id.play_recording_linear_layout2);
//        playRecordingLinearLayout3 = root.findViewById(R.id.play_recording_linear_layout3);
//
//        playRecordingImageView1 = root.findViewById(R.id.play_recording_image_view1);
//        playRecordingImageView2 = root.findViewById(R.id.play_recording_image_view2);
//        playRecordingImageView3 = root.findViewById(R.id.play_recording_image_view3);
//        playRecordingImageView1.setOnClickListener(this);
//        playRecordingImageView2.setOnClickListener(this);
//        playRecordingImageView3.setOnClickListener(this);
//
//        recordingSeekBar1 = root.findViewById(R.id.playing_recording_seek_bar1);
//        recordingSeekBar2 = root.findViewById(R.id.playing_recording_seek_bar2);
//        recordingSeekBar3 = root.findViewById(R.id.playing_recording_seek_bar3);
//
//
//        deleteRecordingImageView1 = root.findViewById(R.id.delete_recording_image_view1);
//        deleteRecordingImageView2 = root.findViewById(R.id.delete_recording_image_view2);
//        deleteRecordingImageView3 = root.findViewById(R.id.delete_recording_image_view3);
//        deleteRecordingImageView1.setOnClickListener(this);
//        deleteRecordingImageView2.setOnClickListener(this);
//        deleteRecordingImageView3.setOnClickListener(this);



    }

//    public void onButtonPressed(Uri uri) {
//        if (mListener != null) {
//            mListener.onFragmentInteraction(uri);
//        }
//    }

    private void configRecorder(){
        audioRecorder = new AudioRecorder(getActivity(),false);
        audioRecorder.setAudioFileObserver(new AudioRecorder.AudioFileObserver() {
            @Override
            public void onWavFileSaved(String filename) {
                Log.i("New Hotword fragment", "onWavFileSaved: ");
                recordedFiles.add(filename);
//                audioExists[recordedFileCount]=true;
//                displayPlayer();
                recordedFileCount++;
                if(recordedFileCount==3){
                    recordHotwordButton.setText("Create Pmdl");
                }else{
                    recordHotwordButton.setText("RECORD "+(3-recordedFileCount)+" more time");
                }
                Toast.makeText(getActivity(), "Audio saved", Toast.LENGTH_SHORT).show();

            }
        });
        audioRecorder.setActivityListener(new AudioRecorder.ActivityListener() {
            @Override
            public void onRecording(String logs) {

            }

            @Override
            public void onAudioChunkPulled(float amplitude) {
                animateVoice(amplitude);
            }
        });
    }

    private void configApiCaller(){
        apiCaller.setApiCallListener(new ApiCaller.ApiCallListener() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                                try {
                    Log.i("Response from backend:",response.toString());
                    String responseCode= response.body().string();

                    if(responseCode.equals("0")){
                        Toast.makeText(getActivity(),"Api request to snowboy failed from backend",Toast.LENGTH_SHORT).show();
                    }else{
                        Toast.makeText(getActivity(), "Pmdl Created and saved to server sucessfully", Toast.LENGTH_SHORT).show();
                    }


                } catch (Exception e) {
                    e.printStackTrace();
                }
                resetRecordedFiles();
                mProgressDialog.dismiss();
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                                mProgressDialog.dismiss();
                resetRecordedFiles();
                Toast.makeText(getActivity(), "Unable to connect backend", Toast.LENGTH_SHORT).show();
                Log.e("New pmdl-->", "call failed");
                t.printStackTrace();
            }
        });
    }
//    private void requestNewPmdlCreation(){
//        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
//        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
//        OkHttpClient client = new OkHttpClient.Builder().addInterceptor(interceptor).build();
//
//        // NewPmdl creation request pmdl
//        NewPmdlAPI newPmdlAPI = RetrofitClientInstance.getRetrofitInstance().create(NewPmdlAPI.class);
//
//        List<MultipartBody.Part> requestBodyParts = new ArrayList<>();
//        RequestBody hotWordName = RequestBody.create(MediaType.parse("text/plain"), hotwordNameEditText.getText().toString());
//        for(int i=0;i<recordedFiles.size();i++){
//            File file = new File(recordedFiles.get(i));
//            RequestBody reqFile = RequestBody.create(MediaType.parse("multipart/form-data"), file);
//            MultipartBody.Part body = MultipartBody.Part.createFormData("file"+(i+1), file.getName(), reqFile);
//            requestBodyParts.add(body);
//            }
//
//        retrofit2.Call<okhttp3.ResponseBody> req = newPmdlAPI.newPmdl(hotWordName,requestBodyParts.get(0),
//                requestBodyParts.get(1),
//                requestBodyParts.get(2));
//
//        req.enqueue(new Callback<ResponseBody>() {
//
//            @Override
//            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
//                try {
//                    Log.i("Response from backend:",response.toString());
//                    String responseCode= response.body().string();
//
//                    if(responseCode.equals("0")){
//                        Toast.makeText(getActivity(),"Api request to snowboy failed from backend",Toast.LENGTH_SHORT).show();
//                    }else{
//                        Toast.makeText(getActivity(), "Pmdl Created and saved to server sucessfully", Toast.LENGTH_SHORT).show();
//                    }
//
//
//                } catch (Exception e) {
//                    e.printStackTrace();
//                }
//                resetRecordedFiles();
//                mProgressDialog.dismiss();
//            }
//
//            @Override
//            public void onFailure(Call<ResponseBody> call, Throwable t) {
//                mProgressDialog.dismiss();
//                resetRecordedFiles();
//                Toast.makeText(getActivity(), "Unable to connect backend", Toast.LENGTH_SHORT).show();
//                Log.e("New pmdl-->", "call failed");
//                t.printStackTrace();
//            }
//
//        });
//    }


//    private void displayPlayer() {
//        if(!isVisible(playRecordingLinearLayout1.getVisibility())){
//            playRecordingLinearLayout1.setVisibility(View.VISIBLE);
//        }else if(!isVisible(playRecordingLinearLayout2.getVisibility())){
//            playRecordingLinearLayout2.setVisibility(View.VISIBLE);
//        }else if(!isVisible(playRecordingLinearLayout3.getVisibility())){
//            playRecordingLinearLayout3.setVisibility(View.VISIBLE);
//        }
//    }

    private boolean isVisible(int i){

        if(i==View.VISIBLE)
            return true;
        else
            return false;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
//        if (context instanceof OnFragmentInteractionListener) {
//            mListener = (OnFragmentInteractionListener) context;
//        } else {
//            throw new RuntimeException(context.toString()
//                    + " must implement OnFragmentInteractionListener");
//        }
        apiCaller = new ApiCaller(getActivity());
        configApiCaller();
        storage = new Storage(getActivity());
        mProgressDialog = new ProgressDialog(getActivity());

    }

    private void animateVoice(float maxPeak) {
        recorderImageView.animate().scaleX(1 + maxPeak).scaleY(1 + maxPeak).setDuration(10).start();
    }

    @Override
    public void onDetach() {
        super.onDetach();
//        mListener = null;
    }

    @Override
    public void onClick(View v) {
//        switch (v.getId()){
//            case R.id.play_recording_image_view1:
//                playAudio(0);
//                playRecordingLinearLayout2.setVisibility(View.INVISIBLE);
//                playRecordingLinearLayout3.setVisibility(View.INVISIBLE);
//                playRecordingImageView1.setImageResource(R.drawable.ic_pause_black_24dp);
//                break;
//            case R.id.play_recording_image_view2:
//                playAudio(1);
//                playRecordingLinearLayout1.setVisibility(View.INVISIBLE);
//                playRecordingLinearLayout3.setVisibility(View.INVISIBLE);
//                playRecordingImageView2.setImageResource(R.drawable.ic_pause_black_24dp);
//                break;
//            case R.id.play_recording_image_view3:
//                playAudio(2);
//                playRecordingLinearLayout1.setVisibility(View.INVISIBLE);
//                playRecordingLinearLayout2.setVisibility(View.INVISIBLE);
//                playRecordingImageView3.setImageResource(R.drawable.ic_pause_black_24dp);
//                break;
//            case R.id.delete_recording_image_view1:
//                deleteRecording(0);
//                playRecordingLinearLayout1.setVisibility(View.INVISIBLE);
//                break;
//            case R.id.delete_recording_image_view2:
//                deleteRecording(1);
//                playRecordingLinearLayout2.setVisibility(View.INVISIBLE);
//                break;
//            case R.id.delete_recording_image_view3:
//                deleteRecording(2);
//                playRecordingLinearLayout3.setVisibility(View.INVISIBLE);
//                break;
//        }
    }

    private void resetRecordedFiles() {
        Log.i("recordedFilesSize", "resetRecordedFiles: "+recordedFiles.size());
        for(int i=0;i<recordedFiles.size();i++){
            Log.i("Deleteting ", "resetRecordedFiles: "+recordedFiles.get(i));
            Log.i("Reset", "resetRecordedFiles: deleting "+recordedFiles.get(i));
            FileUtils.deleteFile(new File(recordedFiles.get(i)));
        }
        recordedFiles.clear();

        recordedFileCount=0;
        hotwordNameEditText.setVisibility(View.VISIBLE);
        recordHotwordButton.setText("Record New Hotword");
//        storage.deleteFile(filePath);
//        recordedFiles.remove(i);
//        recordedFileCount--;
//        audioExists[i]=true;
//        if(recordedFileCount<3){
//            recordHotwordButton.setClickable(true);
//            createPmdlButton.setClickable(false);
//        }
    }


    public interface OnFragmentInteractionListener {
        void onFragmentInteraction();
    }

 //   private void playAudio(final int i){
//        mMediaPlayer = new MediaPlayer();
//
//        try {
//            mMediaPlayer.setDataSource(recordedFiles.get(i));
//            int duration = mMediaPlayer.getDuration();
//            switch (i){
//                case 0:
//                    recordingSeekBar1.setMax(duration);
//                    break;
//                case 1:
//                    recordingSeekBar2.setMax(duration);
//                    break;
//                case 2:
//                    recordingSeekBar3.setMax(duration);
//                    break;
//            }
//            mMediaPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
//                @Override
//                public void onCompletion(MediaPlayer mp) {
//                    mp.release();
//                    switch (i){
//                        case 0:
//                            playRecordingImageView1.setImageResource(R.drawable.ic_play_arrow_primary_24dp);
//                            if(audioExists[1]&&audioExists[2]) {
//                                playRecordingLinearLayout2.setVisibility(View.VISIBLE);
//                                playRecordingLinearLayout3.setVisibility(View.VISIBLE);
//                            }
//                            break;
//                        case 1:
//                            playRecordingImageView2.setImageResource(R.drawable.ic_play_arrow_primary_24dp);
//                            if(audioExists[0]&&audioExists[2]) {
//                                playRecordingLinearLayout1.setVisibility(View.VISIBLE);
//                                playRecordingLinearLayout3.setVisibility(View.VISIBLE);
//                            }
//                            break;
//                        case 2:
//                            playRecordingImageView3.setImageResource(R.drawable.ic_play_arrow_primary_24dp);
//                            if(audioExists[0]&&audioExists[1]) {
//                                playRecordingLinearLayout1.setVisibility(View.VISIBLE);
//                                playRecordingLinearLayout2.setVisibility(View.VISIBLE);
//                            }
//                            break;
//                    }
//                }
//            });
//            currentlyPlayingAudio=i;
//            mMediaPlayer.prepare();
//            mMediaPlayer.start();
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
//    }

}
