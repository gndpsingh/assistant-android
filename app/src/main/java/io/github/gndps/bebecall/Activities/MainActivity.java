package io.github.gndps.bebecall.Activities;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.media.AudioFormat;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.MediaRecorder;
import android.net.Uri;
import android.os.Environment;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.github.kayvannj.permission_utils.Func;
import com.github.kayvannj.permission_utils.PermissionUtil;
import com.mapzen.speakerbox.Speakerbox;

import java.io.File;
import java.io.IOException;
import java.util.Calendar;
import java.util.Date;

import io.github.gndps.bebecall.Interfaces.DetectionApi;
import io.github.gndps.bebecall.R;
import io.github.gndps.bebecall.Networking.RetrofitClientInstance;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import okhttp3.logging.HttpLoggingInterceptor;
import omrecorder.AudioChunk;
import omrecorder.AudioRecordConfig;
import omrecorder.OmRecorder;
import omrecorder.PullTransport;
import omrecorder.PullableSource;
import omrecorder.Recorder;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity {

    Button buttonRecord;
    Button buttonPlay;
    private PermissionUtil.PermissionRequestObject mRequestObject;
    MediaRecorder myAudioRecorder;
    boolean mStartRecording;
    boolean mStartPlaying;
    Recorder recorder;
    ImageView recordButton;
    TextView textView;
    private int serverMode;
    private CheckBox localHostCheckBox;

    private Button continousRecordingButton;
    private Button newHotwordButton;

    private static final String LOG_TAG = "AudioRecordTest";
    private static final int REQUEST_RECORD_AUDIO_PERMISSION = 200;
    private static String mFileName = null;

    private MediaRecorder mRecorder = null;

    private MediaPlayer mPlayer = null;
    private AudioManager audioManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initViews();
        createPermissionsObject();
        // Record to the external cache directory for visibility
        mFileName = getExternalCacheDir().getAbsolutePath();
        mFileName += "/file-testing.wav";
        configPlayButton();
        configRecordButton();
        configureRecorder();
        configContinousRecordingButton();
        configNewHotwordButton();
    }

    private void configNewHotwordButton() {
        newHotwordButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(),HotWordActivity.class);
                startActivity(intent);
            }
        });
    }

    private void configContinousRecordingButton(){
        View.OnClickListener listener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
             Intent intent = new Intent(getApplicationContext(),RecorderActivity.class);
             startActivity(intent);
            }
        };
        continousRecordingButton.setOnClickListener(listener);
    }

    private void createPermissionsObject() {
        mRequestObject = PermissionUtil.with(this).request(Manifest.permission.RECORD_AUDIO,
                Manifest.permission.WRITE_EXTERNAL_STORAGE,Manifest.permission.MODIFY_AUDIO_SETTINGS).onAllGranted(
                new Func() {
                    @Override
                    protected void call() {
                        buttonRecord.setEnabled(true);
                    }
                }).onAnyDenied(
                new Func() {
                    @Override
                    protected void call() {
                        buttonRecord.setEnabled(false);
                    }
                }).ask(100); // REQUEST_CODE_STORAGE is what ever int you want (should be distinct)
    }

    private void initViews() {
        buttonRecord = findViewById(R.id.button_record);
        buttonPlay = findViewById(R.id.button_play);
        recordButton = findViewById(R.id.recordButton);
        textView = findViewById(R.id.textView);
        continousRecordingButton = findViewById(R.id.continous_recording_button);
        newHotwordButton = findViewById(R.id.new_hotword_button);
        localHostCheckBox = findViewById(R.id.checkBox);
        localHostCheckBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked){
                    RetrofitClientInstance.changeApiBaseUrl("http://192.168.43.173:8000/");
                    Toast.makeText(MainActivity.this, "Using LocalHost Backend", Toast.LENGTH_SHORT).show();
                }else{
                    RetrofitClientInstance.changeApiBaseUrl("http:/139.59.15.144:80/");
                    Toast.makeText(MainActivity.this, "Using Remote Server Backend", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        mRequestObject.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    private void onRecord(boolean start) {
        if (start) {
            startRecording();
        } else {
            stopRecording();
        }
    }

    private void onPlay(boolean start) {
        if (start) {
            startPlaying();
        } else {
            stopPlaying();
        }
    }


    private void startPlaying() {
        mStartPlaying = true;
        mPlayer = new MediaPlayer();
        try {
            mPlayer.setDataSource(mFileName);
            mPlayer.prepare();
            mPlayer.start();
        } catch (IOException e) {
            Log.e(LOG_TAG, "prepare() failed");
        }
    }

    private void stopPlaying() {
        mStartPlaying = false;
        mPlayer.release();
        mPlayer = null;
    }

    private void startRecording() {
        mStartRecording = true;
        recorder.startRecording();
        /*mRecorder = new MediaRecorder();
        mRecorder.setAudioSource(MediaRecorder.AudioSource.MIC);
        mRecorder.setOutputFormat(MediaRecorder.OutputFormat.THREE_GPP);
        mRecorder.setOutputFile(mFileName);
        mRecorder.setAudioEncoder(MediaRecorder.AudioEncoder.AMR_NB);

        try {
            mRecorder.prepare();
        } catch (IOException e) {
            Log.e(LOG_TAG, "prepare() failed");
        }

        mRecorder.start();*/
    }


    private void stopRecording() {
        mStartRecording = false;
        try {
            recorder.stopRecording();
        } catch (IOException e) {
            Log.e(LOG_TAG, "recorder just couldn't stop");
        }
        configureRecorder();
        detectSound(null);
        /*mRecorder.stop();
        mRecorder.release();
        mRecorder = null;*/
    }

    private void configureRecorder() {
        recorder = OmRecorder.wav(
                new PullTransport.Default(mic(), new PullTransport.OnAudioChunkPulledListener() {
                    @Override
                    public void onAudioChunkPulled(AudioChunk audioChunk) {
                        animateVoice((float) (audioChunk.maxAmplitude() / 200.0));
                    }
                }), file());
    }

    private void animateVoice(final float maxPeak) {
        recordButton.animate().scaleX(1 + maxPeak).scaleY(1 + maxPeak).setDuration(10).start();
    }

    @NonNull
    private File file() {
        File file = new File(Environment.getExternalStorageDirectory(), "file-testing.wav");
        mFileName = file.getAbsolutePath();
        return file;
    }

    @NonNull
    private PullableSource mic() {
        return new PullableSource.Default(
                new AudioRecordConfig.Default(
                        MediaRecorder.AudioSource.MIC, AudioFormat.ENCODING_PCM_16BIT,
                        AudioFormat.CHANNEL_IN_MONO, 16000
                )
        );
    }

    public void configRecordButton() {
        mStartRecording = true;
        View.OnClickListener clicker = new View.OnClickListener() {
            public void onClick(View v) {
                onRecord(mStartRecording);
                if (mStartRecording) {
                    buttonRecord.setText("Stop recording");
                } else {
                    buttonRecord.setText("Start recording");
                }
                mStartRecording = !mStartRecording;
            }
        };
        buttonRecord.setOnClickListener(clicker);
    }

    public void configPlayButton() {
        mStartPlaying = true;
        View.OnClickListener clicker = new View.OnClickListener() {
            public void onClick(View v) {
                onPlay(mStartPlaying);
                if (mStartPlaying) {
                    buttonPlay.setText("Stop playing");
                } else {
                    buttonPlay.setText("Start playing");
                }
                mStartPlaying = !mStartPlaying;
            }
        };
        buttonPlay.setOnClickListener(clicker);
    }

    @Override
    public void onStop() {
        super.onStop();
        if (mRecorder != null) {
            mRecorder.release();
            mRecorder = null;
        }

        if (mPlayer != null) {
            mPlayer.release();
            mPlayer = null;
        }
    }

    public void detectSound(View v) {
        Log.d(LOG_TAG, mFileName);
        uploadFile();
    }

    private void uploadFile() {
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient client = new OkHttpClient.Builder().addInterceptor(interceptor).build();

        // Change base URL to your upload server URL.
        DetectionApi service = RetrofitClientInstance.getRetrofitInstance().create(DetectionApi.class);

        File file = new File(mFileName);

        RequestBody reqFile = RequestBody.create(MediaType.parse("multipart/form-data"), file);
        MultipartBody.Part body = MultipartBody.Part.createFormData("file", file.getName(), reqFile);
        RequestBody name = RequestBody.create(MediaType.parse("text/plain"), file.getName());

        retrofit2.Call<okhttp3.ResponseBody> req = service.detectAudio(name, body);
        req.enqueue(new Callback<ResponseBody>() {

            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                try {
                    Log.i("Response from backend:",response.toString());
                    int responseCode = Integer.parseInt(response.body().string());
                    Log.i("response code:",String.valueOf(responseCode));
                    actCool(responseCode);
                } catch (Exception e) {
                    Log.d(LOG_TAG, "chakko response - error ");
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Log.e(LOG_TAG, "call failed");
                t.printStackTrace();
            }

        });
    }

    private void actCool(int responseCode) {
        switch (responseCode) {
            case 1:
                //time ki hoya
                speakCurrentTime();
                break;
            case 2:
                //happy nu phone la
                callPhone("8447661306", "calling happy raj");
                break;
            case 3:
                //dk nu phone la
                callPhone("9650666435", "calling dk");
                break;
            case 4:
                //gundeep
                callPhone("7838048909", "Calling Gundeep..");
                break;
            case 5:
                 speak("kare aan");
                 break;
            case 6:
                speak("No");
                break;
            case 7:
                speak("Snowboy");
                break;
            case 8:
                speak("smart mirror");
                break;
            case 9:
                speak("Alexa");
                break;
                // oye
            case 10:
                speak("Han be");
                break;
                // time bata
            case 11:
                speakCurrentTime();
                break;
                // or bhai
            case 12:
                speak("Sorry i am a female voice");
                break;

            default:
                textView.setText("can't recognize");
                speak("can't recognize");
        }
    }

    private void speak(String msg) {
        Speakerbox speakerbox = new Speakerbox(getApplication());
        speakerbox.play(msg);
        textView.setText(msg);
    }

    private void callPhone(String phoneNumber, String statusText) {
        textView.setText(statusText);
        Uri number2 = Uri.parse("tel:" + phoneNumber);
        Intent callIntent2 = new Intent(Intent.ACTION_CALL, number2);
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        startActivity(callIntent2);
        final Handler mHandler = new Handler();
        mHandler.postDelayed(new Runnable() {
            @Override
            public void run() {
                audioManager.setMode(AudioManager.MODE_IN_CALL);
                audioManager.setSpeakerphoneOn(true);
            }
        }, 3000);
    }

    private void speakCurrentTime() {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(new Date());
        int hours = calendar.get(Calendar.HOUR_OF_DAY);
        int minutes = calendar.get(Calendar.MINUTE);
        String ampm = calendar.get(Calendar.AM_PM) == 0 ? "am" : "pm";
        if (hours > 12) {
            hours = hours - 12;
        }
        String time_string = "" + hours + ' ' + minutes + ' ' + ampm;
        Speakerbox speakerbox = new Speakerbox(getApplication());
        speakerbox.play(time_string);
        textView.setText(time_string);
    }

}
