package io.github.gndps.bebecall.Obsolute;

import android.content.Context;
import android.media.AudioFormat;
import android.media.AudioRecord;
import android.media.MediaRecorder;
import android.os.AsyncTask;
import android.os.Environment;
import android.os.Handler;
import android.util.Log;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

public class ContinousAudioRecorder {

    private static String TAG = ContinousAudioRecorder.class.getSimpleName();
    private static final int RECORDER_BPP = 16;
    private static final String AUDIO_RECORDER_FILE_EXT_WAV = ".wav";
    private static final String AUDIO_RECORDER_FOLDER = "AudioRecorder";
    private static final String AUDIO_RECORDER_TEMP_FILE = "record_temp.raw";
    private static final int SILENCE_TIME = 1000;

    public enum State {
        INITIALIZING,
        READY,
        RECORDING,
        ERROR,
        STOPPED
    }

    private Context context;
    private State state;

    private int frequency;
    private int channelConfiguration;
    private int audioEncoding;
    private int bufferSize;
    private int source;
    private AudioRecord audioRecord;
    private RecordAudio recordTask;
    private long fistMomentOfPause;
    private short threshold=4000;
    private FileOutputStream os;
    private RecorderActivityListener activityListener;


    public ContinousAudioRecorder(Context context) {

        this.context = context;

        this.frequency = 44100;

        this.audioEncoding = AudioFormat.ENCODING_PCM_16BIT;

        this.channelConfiguration = AudioFormat.CHANNEL_CONFIGURATION_MONO;

        this.source = MediaRecorder.AudioSource.DEFAULT;

        try {

            bufferSize = AudioRecord.getMinBufferSize(frequency,
                    channelConfiguration, audioEncoding);

            audioRecord = new AudioRecord(source, frequency, channelConfiguration, audioEncoding, bufferSize);

            //audioRecord.setRecordPositionUpdateListener(updateListener);

            //audioRecord.setPositionNotificationPeriod(framePeriod);
            this.state = State.INITIALIZING;

        } catch (Exception ex) {

            ex.printStackTrace();

            this.state = State.ERROR;
        }
    }

//    private AudioRecord.OnRecordPositionUpdateListener updateListener = new AudioRecord.OnRecordPositionUpdateListener() {
//
//        @Override
//        public void onMarkerReached(AudioRecord recorder) {
//
//        }
//
//        @Override
//        public void onPeriodicNotification(AudioRecord recorder) {
////          Log.d(Constant.APP_LOG,"Into Periodic Notification...");
//
//
//        }
//
//
//    };


    public void start() {

        if (state == State.INITIALIZING) {

            state = State.RECORDING;

            Handler handler = new Handler();
            handler.postDelayed(new Runnable() {
                public void run() {

                    recordTask = new RecordAudio();
                    recordTask.execute();

                }
            }, 500);

        } else {
            Log.e(ContinousAudioRecorder.class.getName(), "start() called on illegal state");
            state = State.ERROR;
        }

    }

    public void stop() {
        if (state == State.RECORDING) {
            recordTask.cancel(true);
            state = State.STOPPED;


        } else {
            Log.e(ContinousAudioRecorder.class.getName(), "stop() called on illegal state");
            state = State.ERROR;
        }
    }

    public void release() {
        if (state == State.RECORDING) {
            stop();
        }
        if (audioRecord != null) {
            audioRecord.release();

        }
    }

    public void reset() {
        try {
            if (state != State.ERROR) {
                release();
            }
        } catch (Exception e) {
            Log.e(TAG, e.getMessage());

            state = State.ERROR;
        }
    }


    public State getState() {
        return state;
    }


    private class RecordAudio extends AsyncTask<Void, Double, Void> {

        @Override
        protected Void doInBackground(Void... arg0) {
            Log.w(TAG, "doInBackground");
            try {

                String filename = getTempFilename();

                try {
                    os = new FileOutputStream(filename);
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                }

                short[] buffer = new short[bufferSize];

                audioRecord.startRecording();


                // Records continously unless the recorder state is set to State.STOPPED

                while (state == State.RECORDING ) {


                    int bufferReadResult = audioRecord.read(buffer, 0, bufferSize);
                    Log.i(TAG, "doInBackground: Reading buffer..");

                    if (AudioRecord.ERROR_INVALID_OPERATION != bufferReadResult) {

                        // Check for signal
                        double amplitude = maxAmplitude(buffer)/200;
                        activityListener.onAudioChunkPulled(amplitude);

                        int foundPeak = searchThreshold(buffer, threshold);
                        if (foundPeak > -1) { //found signal
                            //record signal
                            Log.i(TAG, "doInBackground: Incoming Audio signal..");

                            fistMomentOfPause = 0; //reset silence time when new sound is detected

                            //double amplitude = getAmplitude(buffer,bufferReadResult);

                            byte[] byteBuffer = ShortToByte(buffer, bufferReadResult);

                            try {
                                Log.i(TAG, "doInBackground: wrting to output stream");
                                os.write(byteBuffer);
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        } else {//count the time
                            //don't save signal
                            Log.i(TAG, "doInBackground: audio didn't crossed threshold");
                            if (fistMomentOfPause == 0) { // Mtlb isse phale wale me signal tha
                                fistMomentOfPause = System.currentTimeMillis(); //measure first moment of silence
                            }


                        }

                        //If no sound signal have been detected for time = SILENCE_TIME
                        if (fistMomentOfPause != 0 && System.currentTimeMillis()-fistMomentOfPause > SILENCE_TIME) {
                            Log.i("Proof", "Long silence detected");
                            // save the file

                            activityListener.onRecording("Silence..");
                            File tempFile = new File(filename);
                            if(tempFile.exists() && tempFile.length() > 0 ) {

                           Log.i(TAG, "saving to wave file");
                                copyWaveFile(filename, getFilename());
                                deleteTempFile();

                               try {
                                   os = new FileOutputStream(filename);
                               } catch (FileNotFoundException e) {
                                   e.printStackTrace();
                               }
                           }
                           fistMomentOfPause = 0;
                       }


                    }

                }

                //close file
                try {
                    os.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }



            } catch (Throwable t) {
                t.printStackTrace();
                Log.e("AudioRecord", "Recording Failed");
            }
            return null;

        } // Do in background

        double getAmplitude(short[] buffer, int count){
            double sum=0;
            for(int i=0;i<count;i++){
                sum += buffer [i] * buffer [i];
            }
            if(count>0)
                return Math.sqrt(sum/count);
            else
                return 0;
        }

        public double maxAmplitude(short[] shorts){
            int nMaxAmp = 0;
            int arrLen = shorts.length;
            int peakIndex;
            for (peakIndex = 0; peakIndex < arrLen; peakIndex++) {
                if (shorts[peakIndex] >= nMaxAmp) {
                    nMaxAmp = shorts[peakIndex];
                }
            }
            return (int) (20 * Math.log10(nMaxAmp / 0.6));
        }

        byte [] ShortToByte(short [] input, int elements) {
            int short_index, byte_index;
            int iterations = elements; //input.length;
            byte [] buffer = new byte[iterations * 2];

            short_index = byte_index = 0;

            for(/*NOP*/; short_index != iterations; /*NOP*/)
            {
                buffer[byte_index]     = (byte) (input[short_index] & 0x00FF);
                buffer[byte_index + 1] = (byte) ((input[short_index] & 0xFF00) >> 8);

                ++short_index; byte_index += 2;
            }

          return buffer;
}

          int  searchThreshold(short[] arr, short thr){
            int peakIndex;
            int arrLen=arr.length;
            for (peakIndex=0;peakIndex<arrLen;peakIndex++){
                if ((arr[peakIndex]>=thr) || (arr[peakIndex]<=-thr)){
                    // some value greater than the thresshold exists in signal
                    return peakIndex;
                }
            }
            return -1; //not found
        }

        private String getFilename(){
            String filepath = Environment.getExternalStorageDirectory().getPath();
            File file = new File(filepath,AUDIO_RECORDER_FOLDER);

            if(!file.exists()){
                file.mkdirs();
            }

            return (file.getAbsolutePath() + "/" + System.currentTimeMillis() + AUDIO_RECORDER_FILE_EXT_WAV);
        }

        private String getTempFilename(){
            String filepath = Environment.getExternalStorageDirectory().getPath();
            File file = new File(filepath,AUDIO_RECORDER_FOLDER);

            if(!file.exists()){
                file.mkdirs();
            }

            File tempFile = new File(filepath,AUDIO_RECORDER_TEMP_FILE);

            if(tempFile.exists())
                tempFile.delete();

            return (file.getAbsolutePath() + "/" + AUDIO_RECORDER_TEMP_FILE);
        }

        private void deleteTempFile() {
            File file = new File(getTempFilename());

            file.delete();
        }

        private void copyWaveFile(String inFilename,String outFilename){
            FileInputStream in = null;
            FileOutputStream out = null;
            long totalAudioLen = 0;
            long totalDataLen = totalAudioLen + 36;
            long longSampleRate = frequency;
            int channels = 1;
            long byteRate = RECORDER_BPP * frequency * channels/8;

            byte[] data = new byte[bufferSize];

            try {
                in = new FileInputStream(inFilename);
                out = new FileOutputStream(outFilename);
                totalAudioLen = in.getChannel().size();
                totalDataLen = totalAudioLen + 36;

                Log.i(TAG, "copyWaveFile: inside copy wavefile");
                WriteWaveFileHeader(out, totalAudioLen, totalDataLen,
                        longSampleRate, channels, byteRate);

                while(in.read(data) != -1){
                    Log.i(TAG, "copyWaveFile: writing to out file");
                    out.write(data);
                }

                in.close();
                out.close();
                Log.i(TAG, "copyWaveFile: file written to output file sucessfully");
                activityListener.onRecording("Sound file created successfully");
                activityListener.onSoundFileCreated(new File(outFilename));
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        private void WriteWaveFileHeader(
                FileOutputStream out, long totalAudioLen,
                long totalDataLen, long longSampleRate, int channels,
                long byteRate) throws IOException {
            Log.i(TAG, "WriteWaveFileHeader: writing the header of wav file");
            byte[] header = new byte[44];

            header[0] = 'R';  // RIFF/WAVE header
            header[1] = 'I';
            header[2] = 'F';
            header[3] = 'F';
            header[4] = (byte) (totalDataLen & 0xff);
            header[5] = (byte) ((totalDataLen >> 8) & 0xff);
            header[6] = (byte) ((totalDataLen >> 16) & 0xff);
            header[7] = (byte) ((totalDataLen >> 24) & 0xff);
            header[8] = 'W';
            header[9] = 'A';
            header[10] = 'V';
            header[11] = 'E';
            header[12] = 'f';  // 'fmt ' chunk
            header[13] = 'm';
            header[14] = 't';
            header[15] = ' ';
            header[16] = 16;  // 4 bytes: size of 'fmt ' chunk
            header[17] = 0;
            header[18] = 0;
            header[19] = 0;
            header[20] = 1;  // format = 1
            header[21] = 0;
            header[22] = (byte) channels;
            header[23] = 0;
            header[24] = (byte) (longSampleRate & 0xff);
            header[25] = (byte) ((longSampleRate >> 8) & 0xff);
            header[26] = (byte) ((longSampleRate >> 16) & 0xff);
            header[27] = (byte) ((longSampleRate >> 24) & 0xff);
            header[28] = (byte) (byteRate & 0xff);
            header[29] = (byte) ((byteRate >> 8) & 0xff);
            header[30] = (byte) ((byteRate >> 16) & 0xff);
            header[31] = (byte) ((byteRate >> 24) & 0xff);
            header[32] = (byte) (channels * 16 / 8);  // block align
            header[33] = 0;
            header[34] = RECORDER_BPP;  // bits per sample
            header[35] = 0;
            header[36] = 'd';
            header[37] = 'a';
            header[38] = 't';
            header[39] = 'a';
            header[40] = (byte) (totalAudioLen & 0xff);
            header[41] = (byte) ((totalAudioLen >> 8) & 0xff);
            header[42] = (byte) ((totalAudioLen >> 16) & 0xff);
            header[43] = (byte) ((totalAudioLen >> 24) & 0xff);

            out.write(header, 0, 44);
            Log.i(TAG, "WriteWaveFileHeader: header written");
        }


        @Override
        protected void onCancelled() {
            super.onCancelled();
            audioRecord.stop();
        }
    } // async task

    public void setRecoderActivityListener(RecorderActivityListener listener){
        this.activityListener = listener;
    }

}
interface RecorderActivityListener{
    void onRecording(String logs);
    void onSoundFileCreated(File file);
    void onAudioChunkPulled(double amplitude);
}