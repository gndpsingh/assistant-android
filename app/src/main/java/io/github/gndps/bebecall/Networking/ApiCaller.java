package io.github.gndps.bebecall.Networking;

import android.content.Context;


import java.io.File;
import java.util.ArrayList;
import java.util.List;

import io.github.gndps.bebecall.Interfaces.NewPmdlAPI;
import io.github.gndps.bebecall.Interfaces.DetectionApi;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ApiCaller {


    private Context mContext;
    private ApiCallListener apiCallListener;
    private DetectionApiFileInProcessingListener fileInProcessingListener;

    public ApiCaller(Context mContext) {
        this.mContext = mContext;
    }

    public void callDetectionApi(String filename) {

        fileInProcessingListener.onFileProcessed(filename);
        DetectionApi service = RetrofitClientInstance.getRetrofitInstance().create(DetectionApi.class);

        File file = new File(filename);

        RequestBody reqFile = RequestBody.create(MediaType.parse("multipart/form-data"), file);
        MultipartBody.Part body = MultipartBody.Part.createFormData("file", file.getName(), reqFile);
        RequestBody name = RequestBody.create(MediaType.parse("text/plain"), file.getName());

        retrofit2.Call<okhttp3.ResponseBody> req = service.detectAudio(name, body);
        performHttpRequest(req);
    }

    public void callNewPmdlApi(String hotwordName,List<String> recordedFiles){

        // NewPmdl creation request pmdl
        NewPmdlAPI newPmdlAPI = RetrofitClientInstance.getRetrofitInstance().create(NewPmdlAPI.class);

        List<MultipartBody.Part> requestBodyParts = new ArrayList<>();
        RequestBody hotWordName = RequestBody.create(MediaType.parse("text/plain"), hotwordName);
        for(int i=0;i<recordedFiles.size();i++){
            File file = new File(recordedFiles.get(i));
            RequestBody reqFile = RequestBody.create(MediaType.parse("multipart/form-data"), file);
            MultipartBody.Part body = MultipartBody.Part.createFormData("file"+(i+1), file.getName(), reqFile);
            requestBodyParts.add(body);
        }

        retrofit2.Call<okhttp3.ResponseBody> req = newPmdlAPI.newPmdl(hotWordName,requestBodyParts.get(0),
                requestBodyParts.get(1),
                requestBodyParts.get(2));

        performHttpRequest(req);

    }

    public void setApiCallListener(ApiCallListener callListener){
        this.apiCallListener = callListener;
    }

    private void performHttpRequest(retrofit2.Call<okhttp3.ResponseBody> request){
        request.enqueue(new Callback<ResponseBody>() {

            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                apiCallListener.onResponse(call,response);

            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                apiCallListener.onFailure(call,t);
            }


        });
    }

    public void setDetectionApiFileInProcessingListener(DetectionApiFileInProcessingListener listener){
        fileInProcessingListener = listener;
    }

    public interface ApiCallListener{
        void onResponse(Call<ResponseBody> call, Response<ResponseBody> response);
        void onFailure(Call<ResponseBody> call, Throwable t);
    }

    public interface  DetectionApiFileInProcessingListener{
        void onFileProcessed(String filePath);
    }

}
